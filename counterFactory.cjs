function counterFactory() {
  let counterVariable = 0;
  function increment() {
    return (counterVariable += 1);
  }
  function decrement() {
    return (counterVariable -= 1);
  }
  return { increment, decrement };
}

module.exports = counterFactory;
