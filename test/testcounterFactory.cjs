const counterFactory = require("../counterFactory.cjs");

const varyCountObject = counterFactory();

console.log(varyCountObject.increment());
console.log(varyCountObject.decrement());
console.log(varyCountObject.increment());
console.log(varyCountObject.increment());
