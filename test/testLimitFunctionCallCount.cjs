const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callBackFunction() {
  console.log("Hello world");
  return null;
}
let internalLimitFunction;
try {
  internalLimitFunction = limitFunctionCallCount(callBackFunction, 3);
} catch (e) {
  console.log(e);
}
console.log(internalLimitFunction(1, 2, 3));
console.log(internalLimitFunction());
