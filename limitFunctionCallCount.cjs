function limitFunctionCallCount(callBackFunction, numberCount) {
  if (callBackFunction === undefined || numberCount === undefined) {
    throw new Error("Parameters passed is insufficient");
  } else {
    function internalLimitFunction(...args) {
      if (numberCount > 0) {
        numberCount -= 1;
        return callBackFunction(...args);
      } else {
        return null;
      }
    }
    return internalLimitFunction;
  }
}

module.exports = limitFunctionCallCount;
