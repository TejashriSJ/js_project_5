function cacheFunction(cacheCallBackFunction) {
  if (cacheCallBackFunction === undefined) {
    throw new Error("Parameters are not given");
  } else {
    const cache = {};
    function internalCacheFunction(...numberofArguments) {
      if (!(numberofArguments in cache)) {
        cache[numberofArguments] = cacheCallBackFunction(...numberofArguments);
        return cache[numberofArguments];
      } else {
        return cache[numberofArguments];
      }
    }
    return internalCacheFunction;
  }
}
module.exports = cacheFunction;
